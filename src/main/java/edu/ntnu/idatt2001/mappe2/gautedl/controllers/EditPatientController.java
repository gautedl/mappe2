package edu.ntnu.idatt2001.mappe2.gautedl.controllers;

import edu.ntnu.idatt2001.mappe2.gautedl.Patient;
import edu.ntnu.idatt2001.mappe2.gautedl.PatientHolder;
import edu.ntnu.idatt2001.mappe2.gautedl.PatientRegister;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * The controller for adding a patient.
 * @author Gaute Degré Lorentsen
 * @version 1.0.0 2021.04.20
 * @since 2021.04.20
 */
public class EditPatientController {
    @FXML
    public TextField firstNameText, lastNameText, SSnumberText;
    public Button cancelButton, saveButton;


    /**
     * Loads a scene
     * @param event
     * @throws IOException
     */
    private void loadScreen(Event event, String screen) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource(screen));
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        stage.setTitle("Patient register");
        stage.setScene(scene);
        stage.setResizable(true);
        stage.show();
    }

    /**
     * Initializes the scene
     */
    @FXML
    private void initialize(){
        Patient patient = PatientHolder.getINSTANCE().getPatient();
        if (patient == null){
            firstNameText.setText("");
            lastNameText.setText("");
            SSnumberText.setText("");
        } else {
            firstNameText.setText(patient.getFirstName());
            lastNameText.setText(patient.getLastName());
            SSnumberText.setText(patient.getSocialSecurityNumber());
        }

    }
    /**
     * Closes the edit menu
     * @param event
     */
    private void closeAddMenu(Event event) {
        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    /**
     * Cancels saving the patient.
     * @param event
     * @throws IOException
     */
    public void cancelSavePatient(ActionEvent event) throws IOException {
        closeAddMenu(event);
    }

    /**
     * Saves the patient to the register
     * Checks if any of the fields are empty and throws a warning.
     * Checks if the ss number is less or greater than 11 and throws a warning.
     * @param event
     * @throws Exception
     */
    public void savePatient(ActionEvent event) throws Exception {
        Patient patient = PatientHolder.getINSTANCE().getPatient();
        Alert alert = new Alert(Alert.AlertType.ERROR, "", ButtonType.OK);
        if (patient == null){
            Patient newPatient = new Patient(SSnumberText.getText(), firstNameText.getText(), lastNameText.getText());

            if(PatientRegister.getPatients().contains(SSnumberText)){
                alert(alert, SSnumberText.getText() + " Already registered.");
                if(alert.getResult() == ButtonType.OK){
                    alert.close();
                }
            }else if (firstNameText.getText().trim().equals("")
                    || lastNameText.getText().trim().equals("") || SSnumberText.getText().trim().equals("")){
                alert(alert, "First name, last name or Social security number can't be empty");
                return;
            }else if(Long.parseLong(SSnumberText.getText()) > 99999999999l || (Long.parseLong(SSnumberText.getText()) < 10000000000l)){
                alert(alert, "Social Security number must be 11 characters long");
                return;
            }
            PatientRegister.addPatient(newPatient);
            closeAddMenu(event);
        } else if (firstNameText.getText().trim().equals("")
                || lastNameText.getText().trim().equals("") || SSnumberText.getText().trim().equals("")){
            alert(alert, "First name, last name or Social security number can't be empty");
            return;
        } else if(Long.parseLong(SSnumberText.getText()) > 99999999999l || (Long.parseLong(SSnumberText.getText()) < 10000000000l)){
            alert(alert, "Social Security number must be 11 characters long");
            return;
        }
        else{
            patient.setFirstName(firstNameText.getText());
            patient.setLastName(lastNameText.getText());
            patient.setSocialSecurityNumber(SSnumberText.getText());

            closeAddMenu(event);
        }
    }

    /**
     *
     * @param alert
     * @param s
     */
    private void alert(Alert alert, String s) {
        alert.setHeaderText(s);
        alert.setResizable(false);
        alert.showAndWait();
    }
}
