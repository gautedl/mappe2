package edu.ntnu.idatt2001.mappe2.gautedl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * The register over all the available patients.
 * @author Gaute Degré Lorentsen
 * @version 1.0.0 2021.04.20
 * @since 2021.04.20
 */
public class PatientRegister {
    private static ArrayList<Patient> patients = new ArrayList<>();
    private static FileHandler reader = new FileHandler();

    public PatientRegister() {
        throw new IllegalArgumentException("This is a utility class");
    }



    /**
     * Adding a patient if patient is not in register.
     * @param patient
     * @throws Exception
     */
    public static void addPatient(Patient patient) throws Exception {
        if(getPatients().contains(patient)){
            throw new Exception("Patient already registered");
        }else {
            getPatients().add(patient);
        }



    }

    /**
     * Get method for patients.
     * @return
     */
    public static ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     * Removes a patient if the patient is in the register.
     * @param patient
     * @throws Exception
     */
    public static void removePatient(Patient patient) throws Exception{
        if (getPatients().contains(patient)){
            getPatients().remove(patient);
        }else {
            throw new Exception("Person not in register.");
        }
    }

    /**
     * Reads from the csv file and adds the patients to a list
     * @param file
     * @return
     */
    public static List<Patient> readFromCSV(String file){
        List<Patient> CSVpatients;
        CSVpatients = reader.readCSVData(file);
        return CSVpatients;
    }

    /**
     * Puts the patients from CSV files into the register.
     * Removes the first index of the list.
     * @param file
     */
    public static void addFromCSV(String file){
        getPatients().addAll(readFromCSV(file));

        //Removes the first lien in the patient.csv file
        for (int i = 0; i < getPatients().size(); i++){
            if (getPatients().get(i).getFirstName().equals("firstName")){
                getPatients().remove(i);
            }
        }
    }

}
