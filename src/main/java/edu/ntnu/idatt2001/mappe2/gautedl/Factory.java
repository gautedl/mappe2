package edu.ntnu.idatt2001.mappe2.gautedl;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

/**
 * This is a factory class to fulfill the assignment
 */
public class Factory {
    public static Node getNode(String nodeType, int x, int y, EventHandler<ActionEvent> onAction) {
        if (nodeType.equalsIgnoreCase("BUTTON")) {
            Button button = new Button();
            button.setLayoutX(x);
            button.setLayoutY(y);
            button.setOnAction(onAction);
            return button;
        } else if (nodeType.equalsIgnoreCase("TABLEVIEW")) {
            TableView tableView = new TableView<>();
            tableView.setLayoutX(x);
            tableView.setLayoutY(y);
            return tableView;
        } else if (nodeType.equalsIgnoreCase("TEXTFIELD")){
            TextField textField = new TextField();
            textField.setLayoutX(x);
            textField.setLayoutY(y);
            return textField;
        } else if (nodeType.equalsIgnoreCase("MENUBAR")){
            MenuBar menuBar = new MenuBar();
            menuBar.setLayoutX(x);
            menuBar.setLayoutY(y);
            return menuBar;
        }else if (nodeType.equalsIgnoreCase("ANCHORPANE")){
            AnchorPane anchorPane = new AnchorPane();
            anchorPane.setLayoutX(x);
            anchorPane.setLayoutY(y);
            return anchorPane;
        } else if (nodeType.equalsIgnoreCase("TEXT")){
            Text text = new Text();
            text.setLayoutX(x);
            text.setLayoutY(y);
            return text;
        } else if (nodeType.equalsIgnoreCase("IMAGEVIEW")){
            ImageView imageView = new ImageView();
            imageView.setLayoutX(x);
            imageView.setLayoutY(y);
            return imageView;
        }

        return null;
    }
}

