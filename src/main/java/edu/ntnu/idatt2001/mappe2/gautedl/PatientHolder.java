package edu.ntnu.idatt2001.mappe2.gautedl;

/**
 * Class for holding a single patient
 */
public final class PatientHolder {
    private Patient patient;
    private final static PatientHolder INSTANCE = new PatientHolder();

    private PatientHolder() {
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public static PatientHolder getINSTANCE() {
        return INSTANCE;
    }
}
