package edu.ntnu.idatt2001.mappe2.gautedl;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * File handler class for importing and exporting csv file.
 */
public class FileHandler {

    /**
     * Adds the patients from the csv file to a list.
     * @param file
     * @return
     */
    public static List<Patient> readCSVData(String file){
        String line = "";
        List<Patient> patients = new ArrayList<>();
         try{
             BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

             while ((line = bufferedReader.readLine()) != null){
                 String[] values = line.split(";");
                 Patient addPatient = new Patient(values[3], values[0], values[1]);
                 patients.add(addPatient);

             }
             bufferedReader.close();
         } catch (FileNotFoundException e) {
             e.printStackTrace();
         } catch (IOException e) {
             e.printStackTrace();
         }

        return patients;
    }

    /**
     * Saves the patients to a csv file
     * @param fileName
     */
    public static void saveToCSV(String fileName){
        Writer writer = null;
        try {
            File file = new File(fileName);
            writer = new BufferedWriter(new FileWriter(file));
            for (Patient patient : PatientRegister.getPatients()){
                String text = patient.getFirstName() + ";" + patient.getLastName() + ";" + patient.getSocialSecurityNumber();
                writer.write(text);
            }
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }



}
