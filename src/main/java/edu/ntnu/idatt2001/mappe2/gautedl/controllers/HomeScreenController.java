package edu.ntnu.idatt2001.mappe2.gautedl.controllers;

import edu.ntnu.idatt2001.mappe2.gautedl.FileHandler;
import edu.ntnu.idatt2001.mappe2.gautedl.Patient;
import edu.ntnu.idatt2001.mappe2.gautedl.PatientHolder;
import edu.ntnu.idatt2001.mappe2.gautedl.PatientRegister;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;

import java.io.IOException;

/**
 * The controller for the home screen.
 * @author Gaute Degré Lorentsen
 * @version 1.0.0 2021.04.20
 * @since 2021.04.20
 */
public class HomeScreenController {
    @FXML
    public TableView<Patient> patientTableView;
    public TableColumn<Patient, String> firstNameColumn, lastNameColumn, SSNumberColumn;
    public ImageView addUser;


    private ObservableList<Patient> patientList = FXCollections.observableArrayList();


    /**
     * Screen loader for buttons.
     * @param event
     * @param screen
     * @throws IOException
     */
    private void loadScreen(Event event, String screen, String title) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource(screen));
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        stage.setTitle(title);
        stage.setScene(scene);
        stage.setResizable(true);
        stage.show();
    }

    /**
     * Screen loader for menu buttons
     * @param event
     * @param screen
     * @throws IOException
     */
    private void loadScreenMenu(Event event, String screen) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource(screen));
        Stage stage = (Stage) addUser.getScene().getWindow();
        Scene scene = new Scene(root);
        stage.setTitle("Patient register");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.showAndWait();
        fillTableView();

    }

    /**
     * Screen loader for the popup scenes
     * @param s
     * @param s2
     * @throws IOException
     */
    private void screenLoaderPopup(String s, String s2) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(s));
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        stage.setTitle(s2);
        stage.setScene(new Scene(root));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.showAndWait();
        fillTableView();
    }

    /**
     * initializes the stage.
     * @throws Exception
     */
    public void initialize() throws Exception{
        fillTableView();

        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        SSNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        patientTableView.setItems(patientList);
        patientTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        keyListener();
        mouseListener();

    }

    /**
     * Fills the table view
     */
    private void fillTableView() {
        patientList.clear();
        patientList.addAll(PatientRegister.getPatients());
    }

    /**
     * deletes a Patient by marking it and press DELETE
     * Edits a patient by marking it and clicking E
     */
    private void mouseListener() {
        patientTableView.setRowFactory(tv -> {
            TableRow<Patient> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 1 && !row.isEmpty() ) {
                    PatientHolder patientHolder = PatientHolder.getINSTANCE();
                    patientHolder.setPatient(patientTableView.getSelectionModel().getSelectedItem());
                    patientTableView.setOnKeyPressed(ev -> {
                        if(ev.getCode().equals(KeyCode.DELETE)){
                            try {
                                deleteButton(ev);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (ev.getCode().equals(KeyCode.E)){
                            try{
                                screenLoaderPopup("/EditPatient.fxml", "Edit Patient");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            });
            return row;
        });
    }

    /**
     * Adds a patient by pressing the A key
     */
    private void keyListener(){
        patientTableView.setOnKeyPressed(ptv -> {
            if(ptv.getCode().equals(KeyCode.A)){
                try {
                    screenLoaderPopup("/EditPatient.fxml", "Add Patient");
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    /**
     * Method for edit selected patient.
     * @param event
     * @throws IOException
     */
    public void editSelectedPatientButton(ActionEvent event) throws IOException {
        if (patientTableView.getSelectionModel().getSelectedItem() != null){
            PatientHolder patientHolder = PatientHolder.getINSTANCE();
            patientHolder.setPatient(patientTableView.getSelectionModel().getSelectedItem());

            screenLoaderPopup("/EditPatient.fxml", "Edit Patient");
        } else {
            AlertPatientNotSelectedDeleteMethod();
        }
    }

    /**
     * Able to delete a Patient by marking it and press DELETE
     * @param event
     * @throws Exception
     */
    public void deleteButton(KeyEvent event) throws Exception {
        Patient patient = PatientHolder.getINSTANCE().getPatient();
        if (PatientHolder.getINSTANCE().getPatient() != null){
            alertDeletePatient(patient);
            loadScreen(event,"/HomeScreen.fxml", "Patient Register");

        }else {
            AlertPatientNotSelectedDeleteMethod();
        }
        PatientHolder.getINSTANCE().setPatient(null);
    }

    /**
     * Alert for patient not selected when trying to remove
     */
    private void AlertPatientNotSelectedDeleteMethod() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "", ButtonType.OK);
        alert.setHeaderText("You need to select a patient first.");
        alert.setResizable(false);
        alert.showAndWait();
    }

    /**
     * Method connect to remove patient button.
     * @param event
     * @throws Exception
     */
    public void removeSelectedPatientButton(ActionEvent event) throws Exception {

        Patient patient = PatientHolder.getINSTANCE().getPatient();
        if (PatientHolder.getINSTANCE().getPatient() != null){
            alertDeletePatient(patient);
            loadScreen(event,"/HomeScreen.fxml", "Patient Register");

        }else {
            AlertPatientNotSelectedDeleteMethod();
        }
        PatientHolder.getINSTANCE().setPatient(null);

    }

    /**
     * Opens a new window where a new patient can be added.
     * @param event
     * @throws IOException
     */
    public void addNewPatientButton(ActionEvent event) throws IOException {
        screenLoaderPopup("/EditPatient.fxml", "Add Patient");

    }


    /**
     * Method for popup with information about the program.
     * @param event
     */
    public void aboutButton(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Created by:\n" + "Gaute Degré Lorentsen", ButtonType.OK);
        alert.setHeaderText("Patient register\n" +
                "Version 1");
        alert.setResizable(false);
        alert.setTitle("About");
        alert.showAndWait();

    }

    /**
     * Method for exiting program. Gives a warning before exiting.
     * @param event
     */
    public void exitButton(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "", ButtonType.YES, ButtonType.NO);
        alert.setHeaderText("Exit program?");
        alert.setResizable(false);
        alert.showAndWait();
        alert.setHeaderText("Exit");
        if(alert.getResult() == ButtonType.YES) {
            Platform.exit();
        }

    }

    /**
     * Exports the patients to a csv file.
     * @param event
     */
    public void exportCSVButton(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("CSV files (*csv)", ".csv")
        );
        File saveFile = fileChooser.showSaveDialog(patientTableView.getScene().getWindow());

        FileHandler.saveToCSV(saveFile.getPath());
    }


    /**
     * Imports the patients from a csv file.
     * @param event
     */
    public void importCSVButton(ActionEvent event) {
        PatientRegister.getPatients().clear();
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(new Stage());
        System.out.println(file.getPath());

        if (fileType(file.getName())){
            PatientRegister.addFromCSV(file.getPath());
            patientTableView.getItems().addAll(PatientRegister.getPatients());
            System.out.println(PatientRegister.getPatients().get(1).getSocialSecurityNumber().trim());
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("The chosen file type is not valid.");
            alert.setResizable(false);
            alert.showAndWait();
            alert.setHeaderText("Exit");
            return;
        }
    }

    /**
     * Method to check if the file type is .csv
     * @param file
     * @return
     */
    public boolean fileType(String file){
        String[] type = file.split("\\.");
        return type[type.length - 1].equals("csv");
    }

    /**
     * Method for adding selected patient from menu bar
     * @param event
     * @throws IOException
     */
    public void addNewPatientMenu(ActionEvent event) throws IOException {
        addNewPatientButton(event);
    }

    /**
     * Method for removing selected patient from menu bar
     * @param event
     * @throws Exception
     */
    public void removeSelectedPatientMenu(ActionEvent event) throws Exception {
        Patient patient = PatientHolder.getINSTANCE().getPatient();
        if (PatientHolder.getINSTANCE().getPatient() != null){
            alertDeletePatient(patient);
            loadScreenMenu(event,"/HomeScreen.fxml");

        }else {
            AlertPatientNotSelectedDeleteMethod();
        }
        PatientHolder.getINSTANCE().setPatient(null);
    }

    /**
     * Popup alert for when user tries to remove patient
     * @param patient
     * @throws Exception
     */
    private void alertDeletePatient(Patient patient) throws Exception {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "", ButtonType.YES, ButtonType.NO);
        alert.setHeaderText("Delete " + patient.getFirstName() + " " + patient.getLastName() + "?");
        alert.setResizable(false);
        alert.showAndWait();
        if (alert.getResult() == ButtonType.YES) {
            PatientRegister.removePatient(patient);
        }
    }

}
