module MappeDel2 {
    requires javafx.controls;
    requires javafx.fxml;

    opens edu.ntnu.idatt2001.mappe2.gautedl.controllers to javafx.fxml;
    exports edu.ntnu.idatt2001.mappe2.gautedl;
}

