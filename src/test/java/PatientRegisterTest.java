import edu.ntnu.idatt2001.mappe2.gautedl.Patient;
import edu.ntnu.idatt2001.mappe2.gautedl.PatientRegister;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;

public class PatientRegisterTest {

    @Test
    @DisplayName("Test for adding a patient to the register, checking if the size increases")
    public void addingPatientToRegisterTestCheckIfListIncreases() throws Exception{
        PatientRegister.getPatients().clear();
        Patient patient = new Patient("1001", "Tom", "Karlsen");
        PatientRegister.addPatient(patient);
        Assertions.assertEquals(1, PatientRegister.getPatients().size());
    }

    @Test
    @DisplayName("Test for adding a patient to the register, checking if the size increases")
    public void addingPatientToRegisterTestChecksIfPatientIsInList() throws Exception{
        PatientRegister.getPatients().clear();
        Patient patient = new Patient("1001", "Tom", "Karlsen");
        PatientRegister.addPatient(patient);
        Assertions.assertTrue(PatientRegister.getPatients().contains(patient));
    }

    @Test
    @DisplayName("Throwing an argument when trying to add patient already in list.")
    public void throwingArgumentWhenTryingToAddPatientAlreadyRegistered() throws Exception{
        PatientRegister.getPatients().clear();
        Patient patient = new Patient("1001", "Tom", "Karlsen");
        PatientRegister.addPatient(patient);
        Assertions.assertThrows(Exception.class, () -> {
            PatientRegister.addPatient(patient);
        });
    }

    @Test
    @DisplayName("Test for deleting a patient from the register.")
    public void deletingPatientFromRegisterTestCheckIfListDecreases() throws Exception{
        PatientRegister.getPatients().clear();
        Patient patient = new Patient("1001", "Tom", "Karlsen");
        PatientRegister.addPatient(patient);
        int sizeList = PatientRegister.getPatients().size();
        PatientRegister.removePatient(patient);
        Assertions.assertEquals(sizeList - 1, PatientRegister.getPatients().size());
    }

    @Test
    @DisplayName("Test for deleting a patient from the register.")
    public void deletingPatientFromRegisterTestCheckIfPatientIsInRegister() throws Exception{
        PatientRegister.getPatients().clear();
        Patient patient = new Patient("1001", "Tom", "Karlsen");
        PatientRegister.addPatient(patient);
        PatientRegister.removePatient(patient);
        Assertions.assertFalse(PatientRegister.getPatients().contains(patient));
    }

    @Test
    @DisplayName("Throwing an argument when trying to delete a patient not in the register")
    public void throwingArgumentWhenTryingToDeletePatientNotInRegister() throws Exception{
        PatientRegister.getPatients().clear();
        Patient patient = new Patient("1001", "Tom", "Karlsen");
        Assertions.assertThrows(Exception.class, () -> {
            PatientRegister.removePatient(patient);
        });
    }



}
