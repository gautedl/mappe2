import edu.ntnu.idatt2001.mappe2.gautedl.FileHandler;
import edu.ntnu.idatt2001.mappe2.gautedl.PatientRegister;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;

import java.nio.file.Files;
import java.nio.file.Path;

public class FileHandlerTest {

    @Test
    @DisplayName("Reading the test csv file")
    public void readCSVTestFile(){
        Assertions.assertEquals("29104300764", FileHandler.readCSVData("src//main//resources//data//Patients.csv").get(1).getSocialSecurityNumber().trim());
    }

    @Test
    @DisplayName("Saving the csv file")
    public void saveCSVTestFile(){
        FileHandler.saveToCSV("testdata//Patients.csv");
        Assertions.assertTrue(Files.exists(Path.of("testdata//Patients.csv")));
    }

}
